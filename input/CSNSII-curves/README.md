This folder contains the curves used for CSNSII@500kW.
"CSNSII-DualHarmonicCurve.dual" is the dual harmonic RF curve without phase sweep during the injection.

CSNSII-cor-d-30-30-m1.paint is the correlated painting curve for the CSNS-II when the preliminary injection scheme is selected. Its falling edge is used for painting. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 30 mm mm to 0. The painting time should be set 0.5 ms.

CSNSII-anticor-u-30-30-m1.paint is the anti-correlated painting curve for the CSNS-II when the preliminary injection scheme is selected. Its rising edge is used for painting. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 0 mm to 30 mm. The painting time should be set 0.51137 ms.

ACBC-05-03.curve is a magnetic field intensity curve for AC-BC. Its starting time is -0.245 ms. Its flat-top width is 0.5 ms and the falling time is 0.3 ms. 

CSNSII-angscan-cor-R-xy-7p5-30.paint is one of the correlated painting curves by using the angle scanning for the CSNS-II. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 7.5 mm to 0 mm, bump-y from 30 mm to 0 mm.

CSNSII-angscan-cor-L-x-10-8p22.paint is one of the correlated painting curves by using the angle scanning for the CSNS-II. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 10 mm to 8.22 mm.

CSNSII-angscan-cor-L-xp-0-3p75.paint is one of the correlated painting curves by using the angle scanning for the CSNS-II. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 0 mrad to -3.75 mrad.

CSNSII-angscan-anti-R-xy-10-30.paint is one of the anti-correlated painting curves by using the angle scanning for the CSNS-II. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 0 mm, bump-y from 30 mm to 0 mm.

CSNSII-angscan-anti-L-x-12p4-10.paint is one of the anti-correlated painting curves by using the angle scanning for the CSNS-II. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 12.4 mm to 10 mm.

CSNSII-angscan-anti-L-xp-5-0.paint is one of the anti-correlated painting curves by using the angle scanning for the CSNS-II. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 5 mrad to 0 mrad.

CSNSII-cor-8680-R-xy-9-15.paint is one of the correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 1 mm, bump-y from 15 mm to 0 mm.

CSNSII-cor-8680-R-xy-9-20.paint is one of the correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 1 mm, bump-y from 20 mm to 0 mm.

CSNSII-cor-8680-R-xy-9-25.paint is one of the correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 1 mm, bump-y from 25 mm to 0 mm.

CSNSII-cor-8680-L-x-10-7p9.paint is one of the correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 10 mm to 7.9 mm.

CSNSII-cor-8680-L-xp-0-4p5.paint is one of the correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 0 mrad to -4.5 mrad.

CSNSII-cor-8087-R-xy-6-30.paint is one of the correlated painting curves for the CSNS-II while the tune (4.80, 4.87) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 4 mm, bump-y from 30 mm to 0 mm.

CSNSII-cor-8087-L-x-10-8p575.paint is one of the correlated painting curves for the CSNS-II while the tune (4.80, 4.87) is used. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 10 mm to 8.575 mm.

CSNSII-cor-8087-L-xp-0-3.paint is one of the correlated painting curves for the CSNS-II while the tune (4.80, 4.87) is used. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 0 mrad to -3 mrad.

CSNSII-cor-4353-R-xy-5-30.paint is one of the correlated painting curves for the CSNS-II while the tune (4.33, 5.30) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 5 mm, bump-y from 30 mm to 0 mm.

CSNSII-cor-4353-L-x-10-8p8.paint is one of the correlated painting curves for the CSNS-II while the tune (4.33, 5.30) is used. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 10 mm to 8.8 mm.

CSNSII-cor-4353-L-xp-0-2p5.paint is one of the correlated painting curves for the CSNS-II while the tune (4.33, 5.30) is used. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 0 mrad to -2.5 mrad.

CSNSII-anti-8680-R-xy-10-30.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 0 mm, bump-y from 30 mm to 0 mm.

CSNSII-anti-8680-L-x-12-10.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 12.4 mm to 10 mm.

CSNSII-anti-8680-L-xp-5-0.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.86, 4.80) is used. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 5 mrad to 0 mrad.

CSNSII-anti-8087-R-xy-7p5-30.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.80, 4.87) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 2.5 mm, bump-y from 30 mm to 0 mm.

CSNSII-anti-8087-L-x-11p78-10.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.80, 4.87) is used. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 11.78 mm to 10 mm.

CSNSII-anti-8087-L-xp-3p75-0.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.80, 4.87) is used. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 3.75 mrad to 0 mrad.

CSNSII-anti-4353-R-xy-5-30.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.33, 5.30) is used. It is the position variation curve of the circulating beam. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 10 mm to 5 mm, bump-y from 30 mm to 0 mm.

CSNSII-anti-4353-L-x-11p2-10.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.33, 5.30) is used. It is the horizontal position variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-x from 11.2 mm to 10 mm.

CSNSII-anti-4353-L-xp-2p5-0.paint is one of the anti-correlated painting curves for the CSNS-II while the tune (4.33, 5.30) is used. It is the horizontal angle variation curve of the injection beam. The injection process is from -0.245 ms to 0.2547 ms, Inj-xp from 2.5 mrad to 0 mrad.



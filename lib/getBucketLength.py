import os
import math
import numpy as np
from lib.read_curve import *
from lib.read_curve import *
from scipy.optimize import root,fsolve

def FunctionOfPhibk2(phis,a2,phi2,phibk2):
    return np.array(np.sin(phibk2)-np.sin(phis)-a2*(np.sin(2*(phibk2-phis)+phi2)-np.sin(phi2)))
def solveTheFunctionOfPhibk2(phis,a2,phi2):
    init_guess = np.array([[math.pi-phis],[math.pi],[0]])
    sol4_fsolve = fsolve(FunctionOfPhibk2,init_guess,args = (phis,a2,phi2))
    return sol4_fsolve[0]

def FunctionOfPhi(phis,phi,V):
    return np.cos(phi) - np.cos(phis) + (phi-phis)*np.sin(phis) -V
def solveTheFunctionOfPhi(phis,V):
    init_guess = np.array([[-math.pi],[-math.pi+phis]])
    sol4_fsolve = fsolve(FunctionOfPhi,init_guess,args = (phis,V))
    return sol4_fsolve[0]

def interp(x, n_tuple, x_tuple, y_tuple):
    """
    Linear interpolation: Given n-tuple + 1 points,
    x_tuple and y_tuple, routine finds y = y_tuple
    at x in x_tuple. Assumes x_tuple is increasing array.
    """
    if x <= x_tuple[0]:
        y = y_tuple[0]
        return y
    if x >= x_tuple[n_tuple]:
        y = y_tuple[n_tuple]
        return y
    dxp = x - x_tuple[0]
    for n in range(n_tuple):
        dxm = dxp
        dxp = x - x_tuple[n + 1]
        dxmp = dxm * dxp
        if dxmp <= 0:
            break
    y = (-dxp * y_tuple[n] + dxm * y_tuple[n + 1]) /\
        (dxm - dxp)
    return y

def cal_dBrho(RFtime,values):
    dBrhoOverTList = []
    dBrhoOverT = (values[1] - values[0])/(RFtime[1]-RFtime[0])
    dBrhoOverTList.append(dBrhoOverT)
    for i in range(len(RFtime)-2):
        dBrhoOverT = (values[i+2]-values[i])/(RFtime[i+2]-RFtime[i])
        dBrhoOverTList.append(dBrhoOverT)
    dBrhoOverT = (values[-1]-values[-2])/(RFtime[-1]-RFtime[-2])
    dBrhoOverTList.append(dBrhoOverT)
    return dBrhoOverTList

def calPhiDeltP(curve):
    latticLength = 227.92
    timeList = []
    bucketLengthList = []
    if curve.split(".")[-1] == 'multi' :
        [RFtime,values,RFV,RFphase,harms] = read_RF_curve(curve)
        dBrhoOverT = cal_dBrho(RFtime,values)
        dBrhoOverT = np.array(dBrhoOverT)
        RFV = np.array(RFV)
        phis = np.arcsin(dBrhoOverT/RFV*latticLength*1e-9)
        for i in range(len(phis)):
            V_rightPoint = - 2*np.cos(phis[i]) + (math.pi - 2*phis[i])*np.sin(phis[i])
            leftPoint = solveTheFunctionOfPhi(phis[i],V_rightPoint) 
            bucketLength = (math.pi - phis[i] - leftPoint)/math.pi/4*latticLength
            bucketLength = "%.12f" % bucketLength
            RFwrite = "%.12f" % RFtime[i]
            timeList.append(float(RFwrite))
            bucketLengthList.append(float(bucketLength))


    elif curve.split(".")[-1] == 'dual' :
        [RFtime,RFV,RFphase,a2,RF2Phase,values] = read_dual_RF_curve(curve)
        dBrhoOverT = cal_dBrho(RFtime,values)
        dBrhoOverT = np.array(dBrhoOverT)
        RFV = np.array(RFV)
        RF2Phase = np.array(RF2Phase)
        a2 = np.array(a2)
        phis = np.arcsin((dBrhoOverT+a2*np.sin(RF2Phase))/RFV*latticLength*1e-9)


        for indLook in range(len(phis)):
            if a2[indLook] <= 0.02:
                phibk2 = math.pi - phis[indLook]
                phibk = np.linspace(-math.pi,math.pi,2048)

                V = np.cos(phibk2) - np.cos(phibk) + (phibk2-phibk)*np.sin(phis[indLook])
                Pup = np.sqrt(2*np.fabs(V))


            else:
                phibk2 = solveTheFunctionOfPhibk2(phis[indLook],a2[indLook],RF2Phase[indLook])
                phibk = np.linspace(-math.pi,math.pi,2048)
                phi2 = RF2Phase[indLook]
                
                V = np.cos(phibk2) - np.cos(phibk) + (phibk2-phibk)*np.sin(phis[indLook]) - a2[indLook]/2*(np.cos(phi2+2*(phibk2-phis[indLook]))-np.cos(phi2+2*(phibk-phis[indLook]))-2*(phibk-phibk2)*np.sin(phi2))
                Pup = np.sqrt(2*np.fabs(V))
 
            leftPoint = phibk[np.argwhere(Pup[0:1024] == min(Pup[0:1024]))]
            bucketLength = float((phibk2 - leftPoint)/math.pi/4*latticLength)

            bucketLength = "%.12f" % bucketLength
            RFwrite = "%.12f" % RFtime[indLook]
            timeList.append(float(RFwrite))
            bucketLengthList.append(float(bucketLength))
    return (timeList,bucketLengthList)
    
